#input oid

#output json
#output token
#output user_id
#output username
#output ttl
#output state

import groovy.sql.Sql;
import java.sql.Connection;
import com.webratio.rtx.db.DBTransaction;
import com.webratio.rtx.db.HibernateService;
import com.webratio.rtx.core.BeanHelper;
import com.fasterxml.jackson.core.JsonEncoding
import com.fasterxml.jackson.core.JsonFactory
import com.fasterxml.jackson.core.JsonGenerationException
import com.fasterxml.jackson.core.JsonGenerator
import com.fasterxml.jackson.databind.JsonMappingException
import java.io.OutputStream
import java.io.ByteArrayOutputStream

//librerie jackson
import com.fasterxml.jackson.core.JsonFactory;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonParser;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.node.ArrayNode;
import com.fasterxml.jackson.databind.node.JsonNodeFactory;
import com.fasterxml.jackson.databind.node.ObjectNode;

println oid
String[] oid = oid
println oid

Connection conn = null
def databaseId = "db1"
def session = getDBSession(databaseId)
def query = null

if(oid.length>0)
{
	query = "select token, user_oid, username, ttl, state from sso_token where oid = " + oid[0]

	def result=null
	def jsonString = null

	def ris=0
	result = session.createSQLQuery(query).list()
	def average = 0;

	try {
	
	    OutputStream out = new ByteArrayOutputStream()
	    
	    JsonFactory jfactory = new JsonFactory();
	
	    /*** write to file ***/
	    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
	    jGenerator.writeStartArray()
	
	    for (r in result){
	    	//write json
	    	jGenerator.writeStartObject()
		   	jGenerator.writeStringField("token", r[0])
		 	jGenerator.writeNumberField("user_id", Integer.parseInt(r[1].toString()))
		 	jGenerator.writeStringField("username", r[2])
		 	jGenerator.writeStringField("ttl", r[3].toString())
		 	jGenerator.writeStringField("state", r[4].equals("0")?"Authentication successful":"Token expired")
		    jGenerator.writeEndObject()
		}
	
		jGenerator.writeEndArray()
	    jGenerator.close()
	
	    json = out.toString()
	
	} catch (JsonGenerationException e) {
	
	    e.printStackTrace();
	
	} catch (JsonMappingException e) {
	
	    e.printStackTrace();
	
	} catch (IOException e) {
	
	    e.printStackTrace();
	
	}
	
	commit(session)
}
else{
	    OutputStream out = new ByteArrayOutputStream()
	    
	    JsonFactory jfactory = new JsonFactory();
	
	    /*** write to file ***/
	    JsonGenerator jGenerator = jfactory.createJsonGenerator(out, JsonEncoding.UTF8);
	    jGenerator.writeStartArray()
	    
		jGenerator.writeStartObject()
		jGenerator.writeStringField("state", "Invalid token")
		jGenerator.writeEndObject()
		
		jGenerator.writeEndArray()
	    jGenerator.close()
	
	    json = out.toString()
}

println json
ArrayNode mapper = new ObjectMapper().readTree(json);
println mapper
return ["json" : mapper]